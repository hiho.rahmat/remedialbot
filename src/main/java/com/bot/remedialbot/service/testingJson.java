package com.bot.remedialbot.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.*;
import com.google.gson.reflect.*;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import org.apache.commons.text.WordUtils;


public class testingJson {
    static String WEATHER_KEY = "&appid=32b43f309dcf8a4685fe071c63a10286";
    static String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=";

    public static void main(String[] args) throws IOException {
        String urlString = WEATHER_URL+"jakarta"+","+"indonesia"+WEATHER_KEY;
        String urlString2 = WEATHER_URL+"dgnbkgfn"+","+"sdfhsfnr"+WEATHER_KEY;
        String json = jsonReader(urlString);
        Map<String,Object> map = jsonToMap(json);
        Map<String,Object> mainMap = jsonToMap(map.get("main").toString());
        Map<String, Object> windMap = jsonToMap(map.get("wind").toString());
        String weatherString = ""+map.get("weather");
        weatherString = weatherString.substring(2,weatherString.length()-2);
        String[] weatherArr = weatherString.split(", ");
        String currentWeather = weatherArr[1].substring(5);

        String err = "{\"cod\":\"404\",\"message\":\"city not found\"}";
        Map<String,Object> errMap = jsonToMap(err);
        String[] t = {"a","b"};
        String t1 = "";
        t1 += t[1];

        Map<String,Object> countryMap = jsonToMap(map.get("sys").toString());
        boolean check = true;
        System.out.println("weather = "+currentWeather);
        System.out.println(countryMap.get("country"));
        System.out.println(errMap.get("message"));
        System.out.println(check);
        System.out.println(getResponseCode(urlString));
        System.out.println(getResponseCode(urlString2));

        Map<String, String> countries = new HashMap<>();
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            countries.put(l.getDisplayCountry(), iso);
        }
        System.out.println(countries.get(WordUtils.capitalize("united states")));
        String weatherMap = map.get("weather").toString();

        System.out.println(getEmbed("https://xkcd.com/155"));
        MessageEvent m = new MessageEvent("replyToken",new UserSource("userID"),new TextMessageContent("messageID","weather"), Instant.now());
        TextMessageContent a = (TextMessageContent) m.getMessage();
        String b = a.getText();
        System.out.println(b);



    }

    public static Map<String,Object> jsonToMap(String json){
        Map<String,Object> map = new Gson().fromJson(
                json, new TypeToken<HashMap<String,Object>>(){}.getType());
        return map;
    }
    public static String jsonReader(String urlString){
        StringBuilder json = new StringBuilder();
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while((line = rd.readLine()) != null){
                json.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static int getResponseCode(String urlString) throws MalformedURLException, IOException {
        URL u = new URL(urlString);
        HttpURLConnection huc =  (HttpURLConnection)  u.openConnection();
        huc.setRequestMethod("GET");
        huc.connect();
        return huc.getResponseCode();
    }

    public static String getEmbed(String url) throws IOException {
        InputStream response = new URL(url).openStream();
        Scanner scanner = new Scanner(response);
        String responseBody = scanner.useDelimiter("\\A").next();
        String embed = responseBody.substring(responseBody.indexOf("<title>") + 7, responseBody.indexOf("</title>"));
        embed = embed.substring(6);
        return embed.replace(' ','_');
    }
}
