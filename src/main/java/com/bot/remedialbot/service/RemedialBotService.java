package com.bot.remedialbot.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.text.WordUtils;
import org.springframework.stereotype.Service;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class RemedialBotService {

    static final String tidakDikenal = "Maaf, command tidak dikenali (ketik \"help\" untuk melihat command.";
    static final String helpMessage = "Ketik \"weather/kota/negara/measurement\" untuk command weather\n" +
            "contoh : weather/jakarta/indonesia/default atau weather/jakarta/indonesia/metric \n" +
            "measurement : default, metric dan imperial\n \n" +
            "Ketik \"xkcd/code image\" untuk command image search\n" +
            "contoh : xkcd/155 atau xkcd/200";

    static final String WEATHER_KEY = "&appid=32b43f309dcf8a4685fe071c63a10286";
    static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    static final String IMAGE_URL = "https://xkcd.com/";
    static final String WEATHER_WRONG_FORMAT = "Maaf format untuk command weather salah, ketik \"help\" untuk melihat command";
    static final String XKCD_WRONG_FORMAT = "Maaf format untuk command xkcd salah, ketik \"help\" untuk melihat command";

    public RemedialBotService(){}

    public String periksaInput(String pesan) throws IOException {
        String[] pesanSplit = pesan.split("/");
        String jawaban = "";
        String kota = "";
        String negara = "";
        String measurement = "";
        String imageID = "";
        switch(pesanSplit[0].toLowerCase()){
            case ("help"):
                jawaban =helpMessage;
                break;
            case ("weather"):
                if(pesanSplit.length!=4)
                    return WEATHER_WRONG_FORMAT;
                kota += pesanSplit[1].toLowerCase();
                negara += pesanSplit[2].toLowerCase();
                measurement += pesanSplit[3].toLowerCase();
                jawaban = weatherReport(kota,negara,measurement);
                break;
            case ("xkcd"):
                if(pesanSplit.length!=2)
                    return XKCD_WRONG_FORMAT;
                imageID += pesanSplit[1].toLowerCase();
                jawaban = imageMaker(imageID);
                break;
            default:
                jawaban = tidakDikenal;
                break;
        }
        return jawaban;
    }

    public String weatherReport(String kota,String negara,String measurement) throws IOException {
        String urlString = WEATHER_URL+kota+","+negara+WEATHER_KEY+"&units="+measurement;

        if(!isNot404(urlString))
            return "Lokasi tidak ditemukan.";

        String json = jsonReader(urlString);
        if(!isCountryMatch(json,negara))
            return "Nama negara salah.";

        String weather,windSpeed,temperature,humidity;
        weather=getWeather(json);
        windSpeed = getWindSpeed(json);
        temperature = getTemperature(json);
        humidity = getHumidity(json);

        return weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidity,measurement);
    }

    public String getWeather(String json){
        String weather="";
        Map<String, Object> map = jsonToMap(json);
        String weatherString = ""+map.get("weather");
        weatherString = weatherString.substring(2,weatherString.length()-2);
        String[] weatherArr = weatherString.split(", ");
        weather = weatherArr[1].substring(5);
        return weather;
    }

    public String getWindSpeed(String json){
        Map<String, Object> map = jsonToMap(json);
        Map<String, Object> windMap = jsonToMap(map.get("wind").toString());
        return windMap.get("speed").toString();
    }

    public String getTemperature(String json){
        Map<String, Object> map = jsonToMap(json);
        Map<String, Object> tempMap = jsonToMap(map.get("main").toString());
        return tempMap.get("temp").toString();
    }

    public String getHumidity(String json){
        Map<String, Object> map = jsonToMap(json);
        Map<String, Object> humidityMap = jsonToMap(map.get("main").toString());
        return humidityMap.get("humidity").toString();
    }

    public boolean isCountryMatch (String json,String countryName){

        Map<String, String> countries = new HashMap<>();
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            countries.put(l.getDisplayCountry(), iso);
        }
        Map<String, Object> map = jsonToMap(json);
        Map<String, Object> countryMap = jsonToMap(map.get("sys").toString());
        String countryIdInput = countryMap.get("country").toString();
        String countryIdSearched = countries.get(WordUtils.capitalizeFully(countryName));
        return countryIdInput.equalsIgnoreCase(countryIdSearched);
    }

    public String weatherReportMaker(String kota, String negara,String weather, String windSpeed,String temperature,String humidity,String measurement){
        String jawaban ="Weather at your position ("+kota+", "+negara+"):\n" +
                "Weather "+weather+"\n" ;
        switch (measurement){
            case ("default"):
                jawaban += "Wind Speed "+windSpeed+" meter/second \n";
                jawaban += "Temperature "+temperature+" Kelvin \n";
                break;
            case("metric"):
                jawaban += "Wind Speed "+windSpeed+" meter/second \n";
                jawaban += "Temperature "+temperature+" Celsius \n";
                break;
            case("imperial"):
                jawaban += "Wind Speed "+windSpeed+" miles/hour \n";
                jawaban += "Temperature "+temperature+" Fahrenheit \n";
                break;
            default:
                return "Wrong measurement format.";
        }
        jawaban += "Humidity "+humidity+" %";
        return jawaban;
    }

    public String jsonReader(String urlString){
        StringBuilder json = new StringBuilder();
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while((line = rd.readLine()) != null){
                json.append(line);
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public Map<String, Object> jsonToMap(String json){
        return new Gson().fromJson(
                json, new TypeToken<HashMap<String,Object>>(){}.getType());
    }

    public boolean isNot404(String urlString) throws IOException {
        URL u = new URL(urlString);
        HttpURLConnection conn =  (HttpURLConnection)  u.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        boolean response = conn.getResponseCode() != 404;
        conn.disconnect();
        return response;
    }

    public String imageMaker (String imageID) throws IOException {
        String urlString = IMAGE_URL+imageID;
        if(!isNot404(urlString))
            return "Image tidak ditemukan.";
        return urlString;
    }
}
