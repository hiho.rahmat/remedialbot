package com.bot.remedialbot.controller;


import com.bot.remedialbot.service.RemedialBotService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RestController
@LineMessageHandler
public class RemedialBotController {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private RemedialBotService remedialBotService;



    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) throws IOException {
        String pesan = messageEvent.getMessage().getText().toLowerCase();
        String replyToken = messageEvent.getReplyToken();

        balasChatDenganJawaban(replyToken, remedialBotService.periksaInput(pesan));
    }

    private void balasChatDenganJawaban(String replyToken, String jawaban){
        TextMessage jawabanDalamBentukTextMessage = new TextMessage(jawaban);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawabanDalamBentukTextMessage))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ada error saat ingin membalas chat");
        }
    }
}
