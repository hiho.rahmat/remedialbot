package com.bot.remedialbot.service;

import com.google.gson.stream.MalformedJsonException;
import org.junit.jupiter.api.Test;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;


class RemedialBotServiceTest {

    private String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private String WEATHER_KEY = "&appid=32b43f309dcf8a4685fe071c63a10286";
    private String IMAGE_URL = "https://xkcd.com/";
    static final String WEATHER_WRONG_FORMAT = "Maaf format untuk command weather salah, ketik \"help\" untuk melihat command";
    static final String XKCD_WRONG_FORMAT = "Maaf format untuk command xkcd salah, ketik \"help\" untuk melihat command";
    static final String tidakDikenal = "Maaf, command tidak dikenali (ketik \"help\" untuk melihat command.";
    static final String helpMessage = "Ketik \"weather/kota/negara/measurement\" untuk command weather\n" +
            "contoh : weather/jakarta/indonesia/default atau weather/jakarta/indonesia/metric \n" +
            "measurement : default, metric dan imperial\n \n" +
            "Ketik \"xkcd/code image\" untuk command image search\n" +
            "contoh : xkcd/155 atau xkcd/200";
    private String kota = "jakarta";
    private String negara = "indonesia";
    private String measurement = "metric";
    private String imageID = "155";
    private String json = "{\"coord\":{\"lon\":106.85,\"lat\":-6.21},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"few clouds\",\"icon\":\"02d\"}],\"base\":\"stations\",\"main\":{\"temp\":304.7,\"feels_like\":303.37,\"temp_min\":303.71,\"temp_max\":305.15,\"pressure\":1007,\"humidity\":46},\"visibility\":7000,\"wind\":{\"speed\":6.2,\"deg\":70},\"clouds\":{\"all\":20},\"dt\":1595752653,\"sys\":{\"type\":1,\"id\":9383,\"country\":\"ID\",\"sunrise\":1595718253,\"sunset\":1595760824},\"timezone\":25200,\"id\":1642911,\"name\":\"Jakarta\",\"cod\":200}";
    private RemedialBotService service = new RemedialBotService();
    private String temperature = "304.7";
    private String weather = "Clouds";
    private String windSpeed = "6.2";
    private String humidity = "46.0";


    @Test
    void periksaInput() throws IOException {
        assertEquals(helpMessage,service.periksaInput("help"));
        assertEquals(WEATHER_WRONG_FORMAT,service.periksaInput("weather/aka"));
        assertNotEquals(WEATHER_WRONG_FORMAT,service.periksaInput("weather/jakarta/indonesia/metric"));
        assertEquals(XKCD_WRONG_FORMAT,service.periksaInput("xkcd/aba/155"));
        assertNotEquals(XKCD_WRONG_FORMAT,service.periksaInput("xkcd/155"));
        assertEquals(tidakDikenal,service.periksaInput("abababa/abababa"));
    }

    @Test
    void weatherReport() throws IOException {
        String wrongMeasurementFormat = "Wrong measurement format.";
        String lokasiNotFound = "Lokasi tidak ditemukan.";
        String negaraSalah = "Nama negara salah.";
        assertEquals(lokasiNotFound,service.weatherReport("qwertsa","qwertsa","metric"));
        assertEquals(negaraSalah,service.weatherReport(kota,"japan",measurement));
        assertEquals(wrongMeasurementFormat,service.weatherReport(kota,negara,"salah"));
        assertNotEquals(lokasiNotFound,service.weatherReport(kota,negara,measurement));
    }

    @Test
    void isCountryMatch(){
        assertTrue(service.isCountryMatch(json,"indonesia"));
        assertFalse(service.isCountryMatch(json,"japan"));
    }

    @Test
    void weatherReportMaker() {
        String weather = "Rain";
        String windSpeed = "12";
        String temperature = "25";
        String humidty = "52";
        assertEquals("Wrong measurement format.",service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,"salah"));
        assertNotEquals("Wrong measurement format.",service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,measurement));
        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,measurement).contains("Weather at your position ("+kota+", "+negara+"):\nWeather "+weather+"\n"));

        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,measurement).contains("Wind Speed " +windSpeed+" meter/second"));
        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,measurement).contains("Temperature "+temperature+" Celsius"));

        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,"default").contains("Temperature "+temperature+" Kelvin"));

        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,"imperial").contains("Wind Speed " +windSpeed+" miles/hour"));
        assertTrue(service.weatherReportMaker(kota,negara,weather,windSpeed,temperature,humidty,"imperial").contains("Temperature "+temperature+" Fahrenheit"));
    }

    @Test
    void getWeather(){
        assertEquals(weather,service.getWeather(json));
    }

    @Test
    void getWindSpeed(){
        assertEquals(windSpeed,service.getWindSpeed(json));
    }

    @Test
    void getTemperature(){
        assertEquals(temperature,service.getTemperature(json));
    }

    @Test
    void getHumidity(){
        assertEquals(humidity,service.getHumidity(json));
    }

    @Test
    void jsonReader() {
        String url = WEATHER_URL+kota+","+negara+WEATHER_KEY+"&units="+measurement;
        assertFalse(service.jsonReader(url).isEmpty());
        assertNotNull(service.jsonReader(url));
    }

    @Test
    void jsonToMap() {
        assertTrue(service.jsonToMap(json).containsKey("main"));
        assertTrue(service.jsonToMap(json).containsKey("weather"));
        assertTrue(service.jsonToMap(json).containsKey("wind"));
    }

    @Test
    void isNot404() throws IOException {
        String urlImage = IMAGE_URL+imageID;
        String urlImageNotFound = IMAGE_URL+"qazwsx";
        assertFalse(service.isNot404(urlImageNotFound));
        assertTrue(service.isNot404(urlImage));
    }

    @Test
    void imageMaker() throws IOException {
        String urlImage = IMAGE_URL+imageID;
        String notFound = "Image tidak ditemukan.";
        assertEquals(notFound,service.imageMaker("qazwsx"));
        assertEquals(urlImage,service.imageMaker(imageID));
    }
}