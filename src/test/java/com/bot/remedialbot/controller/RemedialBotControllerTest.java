package com.bot.remedialbot.controller;

import com.bot.remedialbot.service.RemedialBotService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RemedialBotControllerTest {

    @Mock
    private LineMessagingClient lineMessagingClient;

    @InjectMocks
    private RemedialBotController remedialBotController;


    @Test
    public void handleHelpMessageTest() throws IOException, ExecutionException, InterruptedException {
        MessageEvent request = new MessageEvent<>("replyToken", new UserSource("userID"), new TextMessageContent("id","weather"), Instant.now());
        String message = ((TextMessageContent) request.getMessage()).toString();


        when(lineMessagingClient
                .replyMessage(new ReplyMessage("reply", new TextMessage(message)))
                .get()).thenReturn(new BotApiResponse("Maaf, command tidak dikenali (ketik \"help\" untuk melihat command.",Collections.emptyList()));

        remedialBotController.handleTextEvent(request);

        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken",new TextMessage(message)));

    }

}